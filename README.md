# Magento2-Demo: Composer Meta Package

```bash
$ composer config repositories.demo.meta vcs git@gitlab.com:sreichel/magento2-demo-composer-meta-package.git
```
---
```bash
$ composer require mse-sv3n/magento2-demo-meta-package
```
